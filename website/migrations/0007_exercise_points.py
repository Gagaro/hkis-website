# Generated by Django 3.1.3 on 2020-12-06 14:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0006_user_public_profile'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='points',
            field=models.IntegerField(default=1),
        ),
    ]
