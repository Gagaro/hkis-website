# Generated by Django 3.1.6 on 2021-04-29 12:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0016_exercise_page'),
    ]

    operations = [
        migrations.RenameField(
            model_name='page',
            old_name='url',
            new_name='slug',
        ),
    ]
